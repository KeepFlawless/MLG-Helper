package de.keepflawless.mlghelper;

import net.labymod.ingamegui.ModuleCategory;
import net.labymod.ingamegui.ModuleCategoryRegistry;
import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement;
import net.labymod.utils.Material;

public class MLGHelperModule extends SimpleModule {

    public static String option;

    public String getDisplayName() {
        return "MLG-Helper";
    }

    public String getDisplayValue() {
        return MLGHelper.returnInformation();
    }

    public String getDefaultValue()
    {
        return "Unbekannt";
    }

    public ControlElement.IconData getIconData() {
        return new ControlElement.IconData(Material.WEB);
    }

    public void loadSettings() {}

    public String getSettingName() {
        return "MLG-Helper";
    }

    public String getDescription() {
        return "Höhenanzeigender MLG-Helper by KeepFlawless (CWBW24)";
    }

    public int getSortingId() {
        return 0;
    }

    public ModuleCategory getCategory() {
        return ModuleCategoryRegistry.CATEGORY_OTHER;
    }
}
